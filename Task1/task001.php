<?php
// Enter the name
echo "Enter your name: " . PHP_EOL;

// Waiting to enter the name
$name = enter();

// Enter your email
echo "Enter your email: " . PHP_EOL;

// Waiting for a mail
$mail = enter();

// This will display the thank you message including their name they entered
echo "Thank you $name." . PHP_EOL;
echo "We will send our promotions to your email: $mail" . PHP_EOL;

// our function to read from the command line
function enter()
{
        $fr=fopen("php://stdin","r");   // open our file pointer to read from s$
        $input = fgets($fr,128);        // read a maximum of 128 characters
        $input = rtrim($input);         // trim any trailing spaces.
        fclose ($fr);                   // close the file handle
        return $input;                  // return the text entered
}
?>