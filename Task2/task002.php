<?php
//List of cities
$cities = array("Tokyo", "Mexico City","New York City", "Mumbai", "Seoul", "Shanghai", "Lagos", "Buenos Aires", "Cairo", "London");

//Counts the number of cities on the list
$b1 = count($cities);

echo "<b>Unordered list:<br></b>";
for($i = 0; $i <  $b1; $i++) 
	//Display cities
	echo $q=$i+1, ". ", $cities[$i], ",<br>";

echo "<br>";

//Existing functions for sorting
sort($cities);

echo "<b>Sorted list of cities[A-Z]:<br></b>";
$b2 = count($cities);
for($j = 0; $j <  $b2; $j++) 
     echo $w=$j+1, ". ", $cities[$j], ",<br>";

echo "<br>";

//Adding new cities to the list
array_push($cities, "Los Angeles", "Calcutta", "Osaka", "Beijing");

sort($cities);

echo "<b>Sorted list of cities with added cities[A-Z]:<br></b>";
$b3 = count($cities);
for($n = 0; $n <  $b3; $n++) 
     echo $e=$n+1, ". ", $cities[$n], ",<br>";
?>
