<?php
    $browser=array('Firefox','Chrome','Internet Explorer','Safari','Opera','Other');
 
    class selections {
        public $selectName;
        public $option;
         
        function __construct ($name, $optList)
		{
            $this->selectName=$name;
            $this->option=$optList;
        }       
        function setName($name)
		{
            $this->selectName=$name;
        }       
		
        function getName()
		{
            return $this->selectName;
        }       
		
        function setValue($optList)
		{

            $this->option=$optList;
        }
		
        function getValue()
		{
            return $this->option;
        }
		
        function createSelections()
		{
            echo "<select name=\"$this->selectName\" >";
            $this->createOptions($this->option);
            echo "</select>";
        }
        function createOptions($optList)
		{       
            foreach($optList as $op)
			{
                echo "<option value=\"$op\">".strtoupper($op)."</option>";
            }
        }   
    }
?>
<h2>Registration form<br/></h2>

<p> Fill The Form: <br/>
 <form action="<?php echo"{$_SERVER['PHP_SELF']}";?>" method="POST">     
        Name:<input name="nameReal" type="text" maxlength="30" /> <br/>
        UserName:<input name="nameUser" type="text" maxlength="30" /> <br/>
        Email:<input name="email" type="text" maxlength="100" /> <br/>
        Browser: <br/>
        <?php 
            $selectField = new selections("browserSelect", $GLOBALS['browser']);
            $selectField->createSelections();
        ?>
        <br/><input type="submit" name="BUTTONES" "/>
    </form>
    
<?php        
        if (isset($_POST['nameReal']) && isset($_POST['nameUser']) && isset($_POST['email']) && isset($_POST['browserSelect']))
		{
            echo "You entered this information: <br/>";
            echo "Name is: ".$_POST['nameReal']."<br/>";
            echo "UserName is: ".$_POST['nameUser']."<br/>";
            echo "email is: ".$_POST['email']."<br/>";
            echo "Browser is: ".$_POST['browserSelect']."<br/>";
           
        }
?>
   