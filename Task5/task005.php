<?php
$multiCity = array(
    array("City", "Country", "Continent"),
    array("Tokyo", "Japan", "Asia"),
    array("Mexico City","Mexico", "North America"),
    array("New York City", "USA", "North America"),
    array("Mumbai", "India", "Asia"),
    array("Seoul", "Korea", "Asia"),
    array("Shanghai", "China", "Asia"),
    array("Lagos", "Nigeria", "Africa"),
    array("Buenos Aires", "Argentina", "South America"),
    array("Cairo", "Egypt", "Africa"),
    array("London", "UK", "Europe")
   
);
?>
 
<head>
<style type="text/css">
td, th {width: 8em; border: 1px solid black; padding-left: 4px;}
th {text-align:center;}
table {border-collapse: collapse; border: 1px solid black;}
</style>
</head>
 
<table>
<thead>
<tr>
<th>
    <?php  //shows the first line in the list
echo $multiCity[0][0] ."</th>\n<th>";  
echo $multiCity[0][1] ."</th>\n<th>";
echo $multiCity[0][2] ."</th>\n";
    ?>
</tr>
</thead>
 
<?php
$num = count($multiCity); // the number of rows in the list
for ($row=1; $row<$num; $row++) //starts at 1 because the first row already added to the table
{
  echo "<tr>\n";
  foreach ($multiCity[$row] as $value)
  {                                        // going through the list and enter the other values in the table
	  
    echo "<td>$value</td>\n";       // put values into table
    }
   echo "</tr>\n";  
}
?>