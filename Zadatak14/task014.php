<?php
$browsers=array
			("None","Firefox","Chrome","Internet Explorer","Safari","Opera","Other");
$speeds=array
			("Unknown","DSL","T1","Cable","Dialup","Other"); 
$os=array
			("Windows","Linux","Macintosh","Other"); 

 class Select 
 {
        private $name;
        private $value;
         
        //Setter
        public function setName($name) 
		{
            $this->name = $name;
        }
        public function setValue($value) 
	{
          
            $this->value = $value;
        }
         
   
        public function getName() 
		{
            return $this->name;
        }
        public function getValue() 
		{
            return $this->value;
        }
         
        public function makeSelect() 
		{
        echo "<select name=\"".$this->getName()."\">";
            $this->makeOptions($this->getValue());
        echo "</select>";
        }
        public function makeOptions($valu) 
		{
            foreach($valu as $va) 
			{
                $this->options($va);
            }
        }
        public function options($v) 
		{
            echo "<option value=\"$v\">".ucfirst($v)."</option>";
        }
        public function __destruct() 
		{
            //echo 'Object was just destroyed <br>';
        }
    }


if (!isset  ($_POST['submit'])) 
{
        echo "<h2>User Registration<br /></h2>";
        echo "<form action=\"task014.php\" method=\"POST\">";
        echo "Name: <input type=\"text\" name=\"name\"  size=\"60\" /><br/>";
        echo "Username: <input type=\"text\" name=\"username\"  size=\"60\" /><br/>";
        echo "Email: <input type=\"text\" name=\"email\"  size=\"60\" /><br/>";
        echo "<p><strong>Work Access</strong></p>";
        echo "Primary Browser: ";
            $browser_work = new Select;
            $browser_work->setName('browser_work');
            $browser_work->setValue($browsers);
            $browser_work->makeSelect();
	
        echo "<br />Connection Speed: ";
            $speed_work = new Select;
            $speed_work->setName('speed_work');
            $speed_work->setValue($speeds);
            $speed_work->makeSelect();
	
        echo "<br />Operating System: ";
            $os_work = new Select;
            $os_work->setName('os_work');
            $os_work->setValue($os);
            $os_work->makeSelect();
	
        echo "<p><strong>Home Access</strong></p>";
        echo "Primary Browser: ";
            $browser_home = new Select;
            $browser_home->setName('browser_home');
            $browser_home->setValue($browsers);
            $browser_home->makeSelect();
	
        echo "<br />Connection Speed: ";
            $speed_home = new Select;
            $speed_home->setName('speed_home');
            $speed_home->setValue($speeds);
            $speed_home->makeSelect();
	
        echo "<br />Operating System: ";
            $ops_home = new Select;
            $ops_home->setName('os_home');
            $ops_home->setValue($os);
            $ops_home->makeSelect();
	
        echo "<br /><input type=\"submit\" name=\"submit\" value=\"Go\" />";
        echo "</form>";
    } 
else 
	{
        $input_local = array($_POST['name'], $_POST['username'], $_POST['email']);
        $work_access = array($_POST['browser_work'], $_POST['speed_work'], $_POST['os_work']);
        $home_access = array($_POST['browser_home'], $_POST['speed_home'], $_POST['os_home']);
        $N = $input_local[0];
        $US = $input_local[1];
	$EM = $input_local[2];

        if (empty($N))
	 {
    	  	die('Error: Please enter your name. <br />
      		<input type="submit" name="back" value="Back to form"
      		onclick="self.location=\'task014.php\'" /></body></html> ');
   	 }

         if (empty($US))
	 {
    	  	die('Error: Please enter your user name. <br />
      		<input type="submit" name="back" value="Back to form"
      		onclick="self.location=\'task014.php\'" /></body></html> ');
   	 }
         
         if (empty($EM))
	 {
    	  	die('Error: Please enter your email. <br />
      		<input type="submit" name="back" value="Back to form"
      		onclick="self.location=\'task014.php\'" /></body></html> ');
   	 }
      
        echo "<p>The following data has been saved for $input_local[0]: </p>\n";
        
        echo "<p>Username: $input_local[1]<br />\n";
        echo "Email: $input_local[2]</p>\n";
         
        echo "Work Access:";
        echo "<ul>";
            foreach($work_access as $value) 
	    {
                echo "<li>".$value ."</li>";
            }
        echo "</ul>";
        echo "Home Access:";
        echo "<ul>";
            foreach($home_access as $value) 
            {
                echo "<li>".$value ."</li>";
            }
        echo "</ul>";
    }

?>